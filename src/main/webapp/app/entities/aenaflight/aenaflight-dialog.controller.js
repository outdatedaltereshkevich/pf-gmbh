(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .controller('AenaflightDialogController', AenaflightDialogController);

    AenaflightDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Aenaflight'];

    function AenaflightDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Aenaflight) {
        var vm = this;

        vm.aenaflight = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.aenaflight.id !== null) {
                Aenaflight.update(vm.aenaflight, onSaveSuccess, onSaveError);
            } else {
                Aenaflight.save(vm.aenaflight, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('testtaskApp:aenaflightUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
