(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .controller('AenaflightDeleteController',AenaflightDeleteController);

    AenaflightDeleteController.$inject = ['$uibModalInstance', 'entity', 'Aenaflight'];

    function AenaflightDeleteController($uibModalInstance, entity, Aenaflight) {
        var vm = this;

        vm.aenaflight = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Aenaflight.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
