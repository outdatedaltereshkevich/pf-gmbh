(function() {
    'use strict';
    angular
        .module('testtaskApp')
        .factory('Aenaflight', Aenaflight);

    Aenaflight.$inject = ['$resource'];

    function Aenaflight ($resource) {
        var resourceUrl =  'api/aenaflights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
