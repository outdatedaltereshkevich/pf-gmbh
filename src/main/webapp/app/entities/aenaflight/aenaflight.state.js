(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('aenaflight', {
            parent: 'entity',
            url: '/aenaflight?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Aenaflights'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/aenaflight/aenaflights.html',
                    controller: 'AenaflightController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('aenaflight-detail', {
            parent: 'aenaflight',
            url: '/aenaflight/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Aenaflight'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/aenaflight/aenaflight-detail.html',
                    controller: 'AenaflightDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Aenaflight', function($stateParams, Aenaflight) {
                    return Aenaflight.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'aenaflight',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('aenaflight-detail.edit', {
            parent: 'aenaflight-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/aenaflight/aenaflight-dialog.html',
                    controller: 'AenaflightDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Aenaflight', function(Aenaflight) {
                            return Aenaflight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('aenaflight.new', {
            parent: 'aenaflight',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/aenaflight/aenaflight-dialog.html',
                    controller: 'AenaflightDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                actArrDateTimeLt: null,
                                aircraftNameScheduled: null,
                                arrAptNameEs: null,
                                arrAptCodeIata: null,
                                baggageInfo: null,
                                carrierAirlineNameEn: null,
                                carrierIcaoCode: null,
                                carrierNumber: null,
                                counter: null,
                                depAptNameEs: null,
                                depAptCodeIata: null,
                                estArrDateTimeLt: null,
                                estDepDateTimeLt: null,
                                flightAirlineNameEn: null,
                                flightAirlineName: null,
                                flightIcaoCode: null,
                                flightNumber: null,
                                fltLegSeqNo: null,
                                gateInfo: null,
                                loungeInfo: null,
                                schdArrOnlyDateLt: null,
                                schdArrOnlyTimeLt: null,
                                sourceData: null,
                                statusInfo: null,
                                terminalInfo: null,
                                arrTerminalInfo: null,
                                createdAt: null,
                                actDepDateTimeLt: null,
                                schdDepOnlyDateLt: null,
                                schdDepOnlyTimeLt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('aenaflight', null, { reload: 'aenaflight' });
                }, function() {
                    $state.go('aenaflight');
                });
            }]
        })
        .state('aenaflight.edit', {
            parent: 'aenaflight',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/aenaflight/aenaflight-dialog.html',
                    controller: 'AenaflightDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Aenaflight', function(Aenaflight) {
                            return Aenaflight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('aenaflight', null, { reload: 'aenaflight' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('aenaflight.delete', {
            parent: 'aenaflight',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/aenaflight/aenaflight-delete-dialog.html',
                    controller: 'AenaflightDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Aenaflight', function(Aenaflight) {
                            return Aenaflight.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('aenaflight', null, { reload: 'aenaflight' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
