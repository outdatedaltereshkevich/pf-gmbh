(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .factory('AenaflightSearch', AenaflightSearch);

    AenaflightSearch.$inject = ['$resource'];

    function AenaflightSearch($resource) {
        var resourceUrl =  'api/_search/aenaflights/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
