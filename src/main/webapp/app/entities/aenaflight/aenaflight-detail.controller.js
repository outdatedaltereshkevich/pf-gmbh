(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .controller('AenaflightDetailController', AenaflightDetailController);

    AenaflightDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Aenaflight'];

    function AenaflightDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Aenaflight) {
        var vm = this;

        vm.aenaflight = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('testtaskApp:aenaflightUpdate', function(event, result) {
            vm.aenaflight = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
