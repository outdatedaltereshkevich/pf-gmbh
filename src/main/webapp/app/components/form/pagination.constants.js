(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
