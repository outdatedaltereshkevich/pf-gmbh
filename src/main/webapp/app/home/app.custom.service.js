( function() {
    "use strict";

    angular
        .module('testtaskApp')
        .service('AppCustomService', AppCustomService);

    AppCustomService.$inject = ['$http'];

    function AppCustomService($http) {
        return {
            loadMetadata: loadMetadata,
            resetData: resetData,
            stopProgress: stopProgress,
            startProgress: startProgress
        };

        function loadMetadata() {
            var url = "api/metadata";
            return $http.get(url);
        }

        function resetData() {
            return $http({
                url: 'api/metadata/reset',
                method: 'POST',
                data: {},
                headers: {'Content-Type': 'application/json'}
            });
        }

        function stopProgress() {
            return $http({
                url: 'api/aenaflights/stop',
                method: 'POST',
                data: {},
                headers: {'Content-Type': 'application/json'}
            });
        }

        function startProgress(count) {
            return $http({
                url: 'api/aenaflights/process/',
                method: 'POST',
                data: {},
                headers: {'Content-Type': 'application/json'}
            });
        }
    }
})();
