(function() {
    'use strict';

    angular
        .module('testtaskApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'AppCustomService', '$interval', 'Auth'];

    function HomeController ($scope, Principal, LoginService, $state, AppCustomService, $interval, Auth) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.metadata = [];
        vm.counter = {};

        vm.resetData = resetData;
        vm.register = register;
        vm.loadData = loadData;
        vm.stopProgress = stopProgress;
        vm.startProgress = startProgress;
        vm.getValues = getValues;

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();
        loadData();

        function loadData() {
            execute();

            $interval( function(){
                execute();
            }, 3000);

            function execute() {
                AppCustomService.loadMetadata().then(function (data) {
                    vm.metadata = data.data;
                    vm.counter = vm.getValues();
                }, function (error) {
                    vm.metadata = [];
                });
            }
        }

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.account === null) {
                    Auth.login({
                        username: 'admin',
                        password: 'admin',
                        rememberMe: true
                    }).then(function () {
                        $state.go('home');
                    }).catch(function () {
                        $state.go('home');
                    });
                }
            });

            function authenticateSuccess (data, status, headers) {
                var bearerToken = headers('Authorization');
                if (angular.isDefined(bearerToken) && bearerToken.slice(0, 7) === 'Bearer ') {
                    var jwt = bearerToken.slice(7, bearerToken.length);
                    service.storeAuthenticationToken(jwt, credentials.rememberMe);
                    return jwt;
                }
            }
        }
        function register () {
            $state.go('register');
        }

        function startProgress() {
            AppCustomService.startProgress(1).then(function (data) {

            }, function (error) {

            });
        }

        function resetData() {
            AppCustomService.resetData().then(function (data) {

            }, function (error) {

            });
        }

        function stopProgress() {
            AppCustomService.stopProgress().then(function (data) {

            }, function (error) {

            });
        }

        function getValues() {
            var counter = {};
            vm.counter['size'] = undefined;
            for (var i = 0; i < vm.metadata.length; i++) {
                if (vm.metadata[i].key === 'SIZE') {
                    counter['size'] = vm.metadata[i].value;
                } else if (vm.metadata[i].key === 'MAX') {
                    counter['max'] = vm.metadata[i].value;
                } else if (vm.metadata[i].key === 'STATE') {
                    counter['state'] = vm.metadata[i].value;
                } else if (vm.metadata[i].key === 'STATUS') {
                    counter['status'] = vm.metadata[i].value;
                }
            }
            if (counter['max'] > 0) {
                counter['percentage'] = (counter['max'] * 100 / counter['size']).toFixed(1);
            } else {
                counter['percentage'] = 0;
            }
            if (counter['percentage'] > 100) {
                counter['percentage'] = 100;
            }
            return counter;
        }
    }
})();
