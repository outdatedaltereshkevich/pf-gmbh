package com.test.service;

import com.test.EntityManagerHelper;
import com.test.domain.Aenaflight;
import com.test.domain.AenaflightSource;
import com.test.domain.AenaflightTemp;
import com.test.domain.Metadata;
import com.test.repository.AenaflightRepository;
import com.test.repository.AenaflightSourceRepository;
import com.test.repository.AenaflightTempRepository;
import com.test.repository.MetadataRepository;
import com.test.service.util.FieldsUtil;
import com.test.service.util.ProcessorUtil;
import org.hibernate.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.swing.plaf.nimbus.State;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
@Transactional
public class AenaflightService {
    public static final String COUNTER_MAX_KEY = "MAX";
    public static final String STATE_KEY = "STATE";
    public static final String SIZE_KEY = "SIZE";
    public static final String STATUS_KEY = "STATUS";

    private final Logger log = LoggerFactory.getLogger(AenaflightService.class);

    @Autowired
    private AenaflightRepository aenaflightRepository;

    @Autowired
    private AenaflightTempRepository aenaflightTempRepository;

    @Autowired
    private MetadataRepository metadataRepository;

    @Autowired
    private AenaflightSourceRepository aenaflightSourceRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private LocalContainerEntityManagerFactoryBean entityManagerFactory;

    @Autowired
    private PlatformTransactionManager transactionManager;

//    @Autowired
//    private SessionFactory sessionFactory;

    /**
     * Save a aenaflight.
     *
     * @param aenaflight the entity to save
     * @return the persisted entity
     */
    public Aenaflight save(Aenaflight aenaflight) {
        log.debug("Request to save Aenaflight : {}", aenaflight);
        Aenaflight result = aenaflightRepository.save(aenaflight);
        return result;
    }

    /**
     * Get all the aenaflights.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Aenaflight> findAll(Pageable pageable) {
        log.debug("Request to get all Aenaflights");
        return aenaflightRepository.findAll(pageable);
    }

    /**
     * Get one aenaflight by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Aenaflight findOne(Long id) {
        log.debug("Request to get Aenaflight : {}", id);
        return aenaflightRepository.findOne(id);
    }

    /**
     * Delete the aenaflight by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Aenaflight : {}", id);
        aenaflightRepository.delete(id);
    }

    public void resetToDefault() {
        aenaflightTempRepository.deleteAll();
        aenaflightSourceRepository.deleteAll();
        //metadataRepository.deleteMetadata(SIZE_KEY);
        metadataRepository.setMetadata(STATUS_KEY, 0L);
        metadataRepository.setMetadata(STATE_KEY, 0L);
        metadataRepository.setMetadata(COUNTER_MAX_KEY, 0L);
    }

    @Transactional(readOnly = true)
    public List<Metadata> findAllMetadata() {
        return metadataRepository.findAll();
    }

    public Metadata getMetadata(String key) {
        Metadata metadata = metadataRepository.findByKey(key);
        if (metadata == null) {
            if (key.equalsIgnoreCase(AenaflightService.SIZE_KEY)) {
                long size = aenaflightRepository.getCount();
                metadata = new Metadata(key, size);
            } else if (key.equalsIgnoreCase(AenaflightService.STATUS_KEY)) {
                metadata = new Metadata(key, 0L);
            } else {
                metadata = new Metadata(key, 0L);
            }
            metadataRepository.save(metadata);
        }
        return metadata;
    }

    public Metadata updateMetadata(Metadata metadata) {
        metadataRepository.save(metadata);
        return metadata;
    }

    public void setState(Long state) {
        metadataRepository.setMetadata(STATE_KEY, state);
    }
    public void saveTempData(Iterable<AenaflightTemp> list) {
        aenaflightTempRepository.save(list);
    }

    @Transactional
    public List<Aenaflight> getPromise(int start, int limit) {
        try {
            SessionFactory sf = entityManagerFactory.getNativeEntityManagerFactory().unwrap(SessionFactory.class);
            StatelessSession session = sf.openStatelessSession();

            try {
                String query = "SELECT a FROM Aenaflight a where a.flightIcaoCode ='IBE' and a.flightNumber='5225' ORDER BY a.id";
                ScrollableResults scrollableResults = session.createQuery(query).setFirstResult(start).setMaxResults(limit).setReadOnly(true).scroll(ScrollMode.FORWARD_ONLY);
                List<Aenaflight> result = new ArrayList<>();
                while (scrollableResults.next()) {
                    Aenaflight entity = (Aenaflight) scrollableResults.get()[0];
                    result.add(entity);
                }
                return result;
            } finally {
                session.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void processToDestinationTable() {
        StatelessSession statelessSession = entityManager.unwrap(Session.class).getSessionFactory().openStatelessSession();
        try {
            String query = "SELECT a FROM AenaflightTemp a";
            ScrollableResults scrollableResults = statelessSession.createQuery(query).setReadOnly(true).scroll(ScrollMode.FORWARD_ONLY);
            while (scrollableResults.next()) {
                AenaflightTemp entity = (AenaflightTemp) scrollableResults.get()[0];
                AenaflightSource source = ProcessorUtil.fromTempValue(entity);
                aenaflightSourceRepository.save(source);
            }
        } finally {
            statelessSession.close();
        }
    }
}
