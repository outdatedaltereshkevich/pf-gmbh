package com.test.service.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

public class FieldsUtil {
    public static LocalDateTime parseDate(String dateValue) {
        List<DateTimeFormatter> knownPatterns = new ArrayList<>();
        knownPatterns.add(createFormatter("dd/MM/yy[ HH:mm:ss]"));
        knownPatterns.add(createFormatter("dd/MM/yyyy[ HH:mm:ss]"));
        knownPatterns.add(createFormatter("yyyy-dd-MM[ HH:mm:ss]"));
        knownPatterns.add(createFormatter("dd/MM/yy HH:mm[:ss]"));
        knownPatterns.add(createFormatter("dd/MM/yyyy HH:mm[:ss]"));
        knownPatterns.add(createFormatter("yyyy-dd-MM HH:mm[:ss]"));

        for (DateTimeFormatter pattern : knownPatterns) {
            try {
                LocalDateTime dt = LocalDateTime.parse(dateValue, pattern);
                return dt;
            } catch (Exception pe) {
                // log error
            }
        }
        return LocalDateTime.now(ZoneId.of("UTC"));
    }

    public static LocalDateTime toDate(Long millis) {
        if (millis == null) {
            millis = Instant.now().toEpochMilli();
        }
        Instant instant = Instant.ofEpochMilli(millis);
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    private static DateTimeFormatter createFormatter(String format) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern(format)
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
            .toFormatter();
        formatter = formatter.withZone(ZoneId.of("UTC"));
        return formatter;
    }

    public static Integer toInteger(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            return 0;
        }
    }
}
