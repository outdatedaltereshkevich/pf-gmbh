package com.test.service.util;

import com.test.domain.Aenaflight;
import com.test.domain.AenaflightSource;
import com.test.domain.AenaflightTemp;

import java.util.*;
import java.util.stream.Collectors;

public class ProcessorUtil {
    public static void processEntity(Map<String, AenaflightTemp> map, Aenaflight entity) {
        if (map == null) {
            map = new HashMap<>();
        }
        String key = entity.getFlightIcaoCode() + "___" + entity.getFlightNumber() + "___" + entity.getSchdDepOnlyDateLt();
        if (map.containsKey(key)) {
            if (entity.getActArrDateTimeLt() != null && !entity.getActArrDateTimeLt().isEmpty()) {
                map.get(key).setActArrDateTimeLt(entity.getActArrDateTimeLt());
            }
            if (entity.getArrAptCodeIata() != null && !entity.getArrAptCodeIata().isEmpty()) {
                map.get(key).setArrAptCodeIata(entity.getArrAptCodeIata());
            }
            if (entity.getAircraftNameScheduled() != null && !entity.getAircraftNameScheduled().isEmpty()) {
                map.get(key).setAircraftNameScheduled(entity.getAircraftNameScheduled());
            }
            if (entity.getArrAptNameEs() != null && !entity.getArrAptNameEs().isEmpty()) {
                map.get(key).setArrAptNameEs(entity.getArrAptNameEs());
            }
            if (entity.getCarrierAirlineNameEn() != null && !entity.getCarrierAirlineNameEn().isEmpty()) {
                map.get(key).setCarrierAirlineNameEn(entity.getCarrierAirlineNameEn());
            }
            if (entity.getCarrierIcaoCode() != null && !entity.getCarrierIcaoCode().isEmpty()) {
                map.get(key).setCarrierIcaoCode(entity.getCarrierIcaoCode());
            }
            if (entity.getCarrierNumber() != null && !entity.getCarrierNumber().isEmpty()) {
                map.get(key).setCarrierNumber(entity.getCarrierNumber());
            }
            if (entity.getDepAptNameEs() != null && !entity.getDepAptNameEs().isEmpty()) {
                map.get(key).setDepAptNameEs(entity.getDepAptNameEs());
            }
            if (entity.getDepAptCodeIata() != null && !entity.getDepAptCodeIata().isEmpty()) {
                map.get(key).setDepAptCodeIata(entity.getDepAptCodeIata());
            }
            if (entity.getEstArrDateTimeLt() != null && !entity.getEstArrDateTimeLt().isEmpty()) {
                map.get(key).setEstArrDateTimeLt(entity.getEstArrDateTimeLt());
            }
            if (entity.getEstDepDateTimeLt() != null && !entity.getEstDepDateTimeLt().isEmpty()) {
                map.get(key).setEstDepDateTimeLt(entity.getEstDepDateTimeLt());
            }
            if (entity.getFlightAirlineNameEn() != null && !entity.getFlightAirlineNameEn().isEmpty()) {
                map.get(key).setFlightAirlineNameEn(entity.getFlightAirlineNameEn());
            }
            if (entity.getFlightAirlineName() != null && !entity.getFlightAirlineName().isEmpty()) {
                map.get(key).setFlightAirlineName(entity.getFlightAirlineName());
            }
            if (entity.getFltLegSeqNo() != null && !entity.getFltLegSeqNo().isEmpty()) {
                map.get(key).setFltLegSeqNo(entity.getFltLegSeqNo());
            }
            if (entity.getSchdArrOnlyDateLt() != null && !entity.getSchdArrOnlyDateLt().isEmpty()) {
                map.get(key).setSchdArrOnlyDateLt(entity.getSchdArrOnlyDateLt());
            }
            if (entity.getSchdArrOnlyTimeLt() != null && !entity.getSchdArrOnlyTimeLt().isEmpty()) {
                map.get(key).setSchdArrOnlyTimeLt(entity.getSchdArrOnlyTimeLt());
            }
            if (entity.getSourceData() != null && !entity.getSourceData().isEmpty()) {
                map.get(key).setSourceData(entity.getSourceData());
            }
            if (entity.getStatusInfo() != null && !entity.getStatusInfo().isEmpty()) {
                map.get(key).setStatusInfo(entity.getStatusInfo());
            }
            if (entity.getActDepDateTimeLt() != null && !entity.getActDepDateTimeLt().isEmpty()) {
                map.get(key).setActDepDateTimeLt(entity.getActDepDateTimeLt());
            }
            if (entity.getSourceData() != null && !entity.getSourceData().isEmpty()) {
                map.get(key).setSourceData(entity.getSourceData());
            }
            if (entity.getSchdDepOnlyTimeLt() != null && !entity.getSchdDepOnlyTimeLt().isEmpty()) {
                map.get(key).setSchdDepOnlyTimeLt(entity.getSchdDepOnlyTimeLt());
            }
            if (notEmptyAndNotNull(entity.getBaggageInfo()) && !hasDuplicates(map.get(key).getBaggageInfo(), entity.getBaggageInfo())) {
                String currentValue = map.get(key).getBaggageInfo();
                map.get(key).setBaggageInfo(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getBaggageInfo());
            }
            if (notEmptyAndNotNull(entity.getCounter()) && !hasDuplicates(map.get(key).getCounter(), entity.getCounter())) {
                String currentValue = map.get(key).getCounter();
                map.get(key).setCounter(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getCounter());
            }
            if (notEmptyAndNotNull(entity.getGateInfo()) && !hasDuplicates(map.get(key).getGateInfo(), entity.getGateInfo())) {
                String currentValue = map.get(key).getGateInfo();
                map.get(key).setGateInfo(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getGateInfo());
            }
            if (notEmptyAndNotNull(entity.getLoungeInfo()) && !hasDuplicates(map.get(key).getLoungeInfo(), entity.getLoungeInfo())) {
                String currentValue = map.get(key).getLoungeInfo();
                map.get(key).setLoungeInfo(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getLoungeInfo());
            }
            if (notEmptyAndNotNull(entity.getTerminalInfo()) && !hasDuplicates(map.get(key).getTerminalInfo(), entity.getTerminalInfo())) {
                String currentValue = map.get(key).getTerminalInfo();
                map.get(key).setTerminalInfo(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getTerminalInfo());
            }
            if (notEmptyAndNotNull(entity.getArrTerminalInfo()) && !hasDuplicates(map.get(key).getArrTerminalInfo(), entity.getArrTerminalInfo())) {
                String currentValue = map.get(key).getArrTerminalInfo();
                map.get(key).setArrTerminalInfo(currentValue + (currentValue.isEmpty() ? "" : ";") + entity.getArrTerminalInfo());
            }
        } else {
            map.put(key, AenaflightTemp.toTempValue(entity));
        }
    }

    public static void main(String[] args) {
        String s = "-";
        String s2 = notEmptyAndNotNull(s) ? s  : "";
        System.out.println(s2.length());
    }

    public static boolean notEmptyAndNotNull(String value) {
        return value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("-");
    }

    private static boolean hasDuplicates(String values, String value) {
        if (values != null) {
            String[] valuesArray = values.split(";");
            return valuesArray.length > 0 && valuesArray[valuesArray.length - 1].equalsIgnoreCase(value);
        }
        return false;
    }

    public static AenaflightSource fromTempValue(AenaflightTemp temp) {
        AenaflightSource source = new AenaflightSource();
        source.setAdep(temp.getDepAptCodeIata());
        source.setAdes(temp.getArrAptCodeIata());
        source.setFlightCode(temp.getKey().getFlightIcaoCode());
        source.setFlightNumber(temp.getKey().getFlightNumber());
        source.setCarrierCode(temp.getCarrierIcaoCode());
        source.setCarrierNumber(temp.getCarrierNumber());
        source.setStatusInfo(temp.getStatusInfo());
        source.setSchdDepLt(FieldsUtil.parseDate(temp.getKey().getSchdDepOnlyDateLt().trim() + " " + temp.getSchdDepOnlyTimeLt().trim()));
        source.setSchdArrLt(FieldsUtil.parseDate(temp.getSchdArrOnlyDateLt().trim() + " " + temp.getSchdArrOnlyTimeLt().trim()));
        source.setEstDepLt(FieldsUtil.parseDate(temp.getEstDepDateTimeLt().trim()));
        source.setEstArrLt(FieldsUtil.parseDate(temp.getEstArrDateTimeLt().trim()));
        source.setActDepLt(FieldsUtil.parseDate(temp.getActDepDateTimeLt().trim()));
        source.setActArrLt(FieldsUtil.parseDate(temp.getActArrDateTimeLt().trim()));
        source.setFltLegSeqNo(FieldsUtil.toInteger(temp.getFltLegSeqNo()));
        source.setAircraftNameScheduled(temp.getAircraftNameScheduled());
        source.setBaggageInfo(revertList(temp.getBaggageInfo()));
        source.setCounter(revertList(temp.getCounter()));
        source.setGateInfo(revertList(temp.getGateInfo()));
        source.setLoungeInfo((temp.getLoungeInfo()));
        source.setTerminalInfo((temp.getTerminalInfo()));
        source.setArrTerminalInfo((temp.getArrTerminalInfo()));
        source.setSourceData(temp.getSourceData());
        source.setCreatedAt(FieldsUtil.toDate(temp.getCreatedAt()));
        return source;
    }

    private static String revertList(String value) {
        if (value != null) {
            String[] array = value.split(";");
            List<String> list = Arrays.asList(array);
            Collections.reverse(list);
            return list.stream().collect(Collectors.joining(";"));
        }
        return "";
    }
}
