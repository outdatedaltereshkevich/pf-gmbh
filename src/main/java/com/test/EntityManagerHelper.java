package com.test;

import org.hibernate.Session;
import org.hibernate.StatelessSession;

import javax.persistence.EntityManager;

public class EntityManagerHelper {
    private static StatelessSession statelessSession;

    public static StatelessSession getSession(EntityManager entityManager) {
        if (statelessSession == null) {
            statelessSession = entityManager.unwrap(Session.class).getSessionFactory().openStatelessSession();
        }
        return statelessSession;
    }
}
