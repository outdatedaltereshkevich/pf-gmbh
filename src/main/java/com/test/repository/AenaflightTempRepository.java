package com.test.repository;

import com.test.domain.AenaflightTemp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AenaflightTempRepository extends JpaRepository<AenaflightTemp, String> {
}
