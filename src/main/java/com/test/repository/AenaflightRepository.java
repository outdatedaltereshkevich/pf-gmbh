package com.test.repository;

import com.test.domain.Aenaflight;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;

@Repository
public interface AenaflightRepository extends JpaRepository<Aenaflight, Long> {
    @Query(value = "select count(*) from aenaflight_2017_01", nativeQuery = true)
    long getCount();

    @Query(value = "select * from aenaflight_2017_01 LIMIT :limit OFFSET :offset", nativeQuery = true)
    List<Aenaflight> gatData(@Param("limit") int limit, @Param("offset") int offset);
}
