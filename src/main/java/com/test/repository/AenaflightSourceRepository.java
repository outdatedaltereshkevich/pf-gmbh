package com.test.repository;

import com.test.domain.AenaflightSource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AenaflightSourceRepository extends JpaRepository<AenaflightSource, String> {
}
