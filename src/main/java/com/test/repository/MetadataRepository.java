package com.test.repository;

import com.test.domain.Metadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MetadataRepository extends JpaRepository<Metadata, Long> {
    @Query("SELECT m FROM Metadata m WHERE m.key = :key")
    Metadata findByKey(@Param("key") String key);

    @Modifying
    @Query("UPDATE Metadata m SET m.value = :state WHERE m.key = :key")
    void setMetadata(@Param("key") String key, @Param("state") Long state);

    @Modifying
    @Query("DELETE FROM Metadata m WHERE m.key = :key")
    void deleteMetadata(@Param("key") String key);
}

