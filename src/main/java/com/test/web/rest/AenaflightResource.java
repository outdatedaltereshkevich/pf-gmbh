package com.test.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.domain.Aenaflight;
import com.test.domain.AenaflightTemp;
import com.test.domain.Metadata;
import com.test.service.AenaflightService;
import com.test.service.util.ProcessorUtil;
import com.test.web.rest.errors.BadRequestAlertException;
import com.test.web.rest.util.HeaderUtil;
import com.test.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Aenaflight.
 */
@RestController
@RequestMapping("/api")
public class AenaflightResource {

    private final Logger log = LoggerFactory.getLogger(AenaflightResource.class);

    private static final String ENTITY_NAME = "aenaflight";

    private final AenaflightService aenaflightService;

    public AenaflightResource(AenaflightService aenaflightService) {
        this.aenaflightService = aenaflightService;
    }

    /**
     * POST  /aenaflights : Create a new aenaflight.
     *
     * @param aenaflight the aenaflight to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aenaflight, or with status 400 (Bad Request) if the aenaflight has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/aenaflights")
    @Timed
    public ResponseEntity<Aenaflight> createAenaflight(@Valid @RequestBody Aenaflight aenaflight) throws URISyntaxException {
        log.debug("REST request to save Aenaflight : {}", aenaflight);
        if (aenaflight.getId() != null) {
            throw new BadRequestAlertException("A new aenaflight cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Aenaflight result = aenaflightService.save(aenaflight);
        return ResponseEntity.created(new URI("/api/aenaflights/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /aenaflights : Updates an existing aenaflight.
     *
     * @param aenaflight the aenaflight to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aenaflight,
     * or with status 400 (Bad Request) if the aenaflight is not valid,
     * or with status 500 (Internal Server Error) if the aenaflight couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/aenaflights")
    @Timed
    public ResponseEntity<Aenaflight> updateAenaflight(@Valid @RequestBody Aenaflight aenaflight) throws URISyntaxException {
        log.debug("REST request to update Aenaflight : {}", aenaflight);
        if (aenaflight.getId() == null) {
            return createAenaflight(aenaflight);
        }
        Aenaflight result = aenaflightService.save(aenaflight);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aenaflight.getId().toString()))
            .body(result);
    }

    /**
     * GET  /aenaflights : get all the aenaflights.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of aenaflights in body
     */
    @GetMapping("/aenaflights")
    @Timed
    public ResponseEntity<List<Aenaflight>> getAllAenaflights(Pageable pageable) {
        log.debug("REST request to get a page of Aenaflights");
        Page<Aenaflight> page = aenaflightService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/aenaflights");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /aenaflights/:id : get the "id" aenaflight.
     *
     * @param id the id of the aenaflight to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aenaflight, or with status 404 (Not Found)
     */
    @GetMapping("/aenaflights/{id}")
    @Timed
    public ResponseEntity<Aenaflight> getAenaflight(@PathVariable Long id) {
        log.debug("REST request to get Aenaflight : {}", id);
        Aenaflight aenaflight = aenaflightService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(aenaflight));
    }

    /**
     * DELETE  /aenaflights/:id : delete the "id" aenaflight.
     *
     * @param id the id of the aenaflight to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/aenaflights/{id}")
    @Timed
    public ResponseEntity<Void> deleteAenaflight(@PathVariable Long id) {
        log.debug("REST request to delete Aenaflight : {}", id);
        aenaflightService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/aenaflights/process/")
    @Timed
    public ResponseEntity<Integer> process() {
        int count = 300000;
        int threadCount = 50000;
        Metadata counterMetadata = aenaflightService.getMetadata(AenaflightService.COUNTER_MAX_KEY);
        Metadata sizeMetadata = aenaflightService.getMetadata(AenaflightService.SIZE_KEY);
        Metadata statusMetadata = aenaflightService.getMetadata(AenaflightService.STATUS_KEY);
        aenaflightService.setState(0L);
        int start = counterMetadata.getValue().intValue();
        long recordsCount = sizeMetadata.getValue();
        Map<String, AenaflightTemp> map = new HashMap<>();

        while (start <= recordsCount) {
            Metadata stateMetadata = aenaflightService.getMetadata(AenaflightService.STATE_KEY);
            if (stateMetadata.getValue() > 0) {
                break;
            }
            map = process(map, count / threadCount, start, count);
            start += count;
            aenaflightService.saveTempData(map.values());
            counterMetadata.setValue((long) start);
            aenaflightService.updateMetadata(counterMetadata);
            System.gc();
        }
        Metadata stateMetadata = aenaflightService.getMetadata(AenaflightService.STATE_KEY);
        if (statusMetadata.getValue() == 0L && stateMetadata.getValue() == 0L) {
            aenaflightService.processToDestinationTable();
            statusMetadata.setValue(1L);
            aenaflightService.updateMetadata(statusMetadata);
        }
        System.gc();
        return ResponseEntity.accepted().build();
    }

    public Map<String, AenaflightTemp> process(Map<String, AenaflightTemp> map, int threads, int start, int limit) {
        try {
            System.out.println(start + " - " + Instant.now().toString());
            ExecutorService executor = Executors.newFixedThreadPool(threads);
            List<Callable<List<Aenaflight>>> callables = new ArrayList<>();
            int step = limit / threads;
            for (int i = start; i < start + limit; i += step) {
                final int startValue = i;
                callables.add(() -> aenaflightService.getPromise(startValue, step));
            }
            List<Future<List<Aenaflight>>> futures = executor.invokeAll(callables);
            List<Aenaflight> list = new ArrayList<>();
            futures.forEach(future -> {
                try {
                    list.addAll(future.get());
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            });
            if (!list.isEmpty()) {
                list.sort(Comparator.comparing(Aenaflight::getCreatedAt));
                list.forEach(entity -> ProcessorUtil.processEntity(map, entity));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<>(map);
    }

    @PostMapping("/aenaflights/stop")
    @Timed
    public ResponseEntity<Void> stopProcessing() {
        aenaflightService.setState(1L);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/metadata")
    @Timed
    public ResponseEntity<List<Metadata>> getAllMetadata() {
        List<Metadata> list = aenaflightService.findAllMetadata();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/metadata/reset")
    @Timed
    public ResponseEntity<Void> resetMetadata() {
        aenaflightService.resetToDefault();
        return ResponseEntity.ok().build();
    }
}
