package com.test.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "metadata")
public class Metadata implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="metadata_id_seq")
    @SequenceGenerator(name="metadata_id_seq", sequenceName="metadata_id_seq", allocationSize=1)
    private Long id;

    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private Long value;

    public Metadata() {

    }

    public Metadata(String key, Long value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
