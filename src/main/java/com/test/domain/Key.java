package com.test.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Embeddable
public class Key implements Serializable {

    @Size(max = 8)
    @Column(name = "flight_icao_code", length = 8)
    private String flightIcaoCode;

    @Size(max = 8)
    @Column(name = "flight_number", length = 8)
    private String flightNumber;

    @Size(max = 32)
    @Column(name = "schd_dep_only_date_lt", length = 32)
    private String schdDepOnlyDateLt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key key = (Key) o;

        if (flightIcaoCode != null ? !flightIcaoCode.equals(key.flightIcaoCode) : key.flightIcaoCode != null)
            return false;
        if (flightNumber != null ? !flightNumber.equals(key.flightNumber) : key.flightNumber != null) return false;
        return schdDepOnlyDateLt != null ? schdDepOnlyDateLt.equals(key.schdDepOnlyDateLt) : key.schdDepOnlyDateLt == null;
    }

    @Override
    public int hashCode() {
        int result = flightIcaoCode != null ? flightIcaoCode.hashCode() : 0;
        result = 31 * result + (flightNumber != null ? flightNumber.hashCode() : 0);
        result = 31 * result + (schdDepOnlyDateLt != null ? schdDepOnlyDateLt.hashCode() : 0);
        return result;
    }

    public String getFlightIcaoCode() {
        return flightIcaoCode;
    }

    public void setFlightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getSchdDepOnlyDateLt() {
        return schdDepOnlyDateLt;
    }

    public void setSchdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
    }
}
