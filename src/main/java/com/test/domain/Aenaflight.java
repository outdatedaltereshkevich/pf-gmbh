package com.test.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "aenaflight_2017_01")
public class Aenaflight implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="aenaflight_2017_01_id_seq")
    @SequenceGenerator(name="aenaflight_2017_01_id_seq", sequenceName="aenaflight_2017_01_id_seq", allocationSize=1)
    private Long id;

    @Size(max = 64)
    @Column(name = "act_arr_date_time_lt", length = 64)
    private String actArrDateTimeLt;

    @Column(columnDefinition="TEXT", name = "aircraft_name_scheduled")
    private String aircraftNameScheduled;

    @Size(max = 128)
    @Column(name = "arr_apt_name_es", length = 128)
    private String arrAptNameEs;

    @Size(max = 8)
    @Column(name = "arr_apt_code_iata", length = 8)
    private String arrAptCodeIata;

    @Size(max = 128)
    @Column(name = "baggage_info", length = 128)
    private String baggageInfo;

    @Size(max = 128)
    @Column(name = "carrier_airline_name_en", length = 128)
    private String carrierAirlineNameEn;

    @Size(max = 8)
    @Column(name = "carrier_icao_code", length = 8)
    private String carrierIcaoCode;

    @Size(max = 8)
    @Column(name = "carrier_number", length = 8)
    private String carrierNumber;

    @Size(max = 64)
    @Column(name = "counter", length = 64)
    private String counter;

    @Size(max = 128)
    @Column(name = "dep_apt_name_es", length = 128)
    private String depAptNameEs;

    @Size(max = 8)
    @Column(name = "dep_apt_code_iata", length = 8)
    private String depAptCodeIata;

    @Size(max = 64)
    @Column(name = "est_arr_date_time_lt", length = 64)
    private String estArrDateTimeLt;

    @Size(max = 64)
    @Column(name = "est_dep_date_time_lt", length = 64)
    private String estDepDateTimeLt;

    @Size(max = 128)
    @Column(name = "flight_airline_name_en", length = 128)
    private String flightAirlineNameEn;

    @Size(max = 128)
    @Column(name = "flight_airline_name", length = 128)
    private String flightAirlineName;

    @Size(max = 8)
    @Column(name = "flight_icao_code", length = 8)
    private String flightIcaoCode;

    @Size(max = 8)
    @Column(name = "flight_number", length = 8)
    private String flightNumber;

    @Size(max = 8)
    @Column(name = "flt_leg_seq_no", length = 8)
    private String fltLegSeqNo;

    @Size(max = 128)
    @Column(name = "gate_info", length = 128)
    private String gateInfo;

    @Size(max = 128)
    @Column(name = "lounge_info", length = 128)
    private String loungeInfo;

    @Size(max = 32)
    @Column(name = "schd_arr_only_date_lt", length = 32)
    private String schdArrOnlyDateLt;

    @Size(max = 32)
    @Column(name = "schd_arr_only_time_lt", length = 32)
    private String schdArrOnlyTimeLt;

    @Column(columnDefinition="TEXT", name = "source_data")
    private String sourceData;

    @Size(max = 128)
    @Column(name = "status_info", length = 128)
    private String statusInfo;

    @Size(max = 128)
    @Column(name = "terminal_info", length = 128)
    private String terminalInfo;

    @Size(max = 128)
    @Column(name = "arr_terminal_info", length = 128)
    private String arrTerminalInfo;

    @Column(name = "created_at")
    private Long createdAt;

    @Size(max = 64)
    @Column(name = "act_dep_date_time_lt", length = 64)
    private String actDepDateTimeLt;

    @Size(max = 32)
    @Column(name = "schd_dep_only_date_lt", length = 32)
    private String schdDepOnlyDateLt;

    @Size(max = 32)
    @Column(name = "schd_dep_only_time_lt", length = 32)
    private String schdDepOnlyTimeLt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActArrDateTimeLt() {
        return actArrDateTimeLt;
    }

    public Aenaflight actArrDateTimeLt(String actArrDateTimeLt) {
        this.actArrDateTimeLt = actArrDateTimeLt;
        return this;
    }

    public void setActArrDateTimeLt(String actArrDateTimeLt) {
        this.actArrDateTimeLt = actArrDateTimeLt;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public Aenaflight aircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
        return this;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    public String getArrAptNameEs() {
        return arrAptNameEs;
    }

    public Aenaflight arrAptNameEs(String arrAptNameEs) {
        this.arrAptNameEs = arrAptNameEs;
        return this;
    }

    public void setArrAptNameEs(String arrAptNameEs) {
        this.arrAptNameEs = arrAptNameEs;
    }

    public String getArrAptCodeIata() {
        return arrAptCodeIata;
    }

    public Aenaflight arrAptCodeIata(String arrAptCodeIata) {
        this.arrAptCodeIata = arrAptCodeIata;
        return this;
    }

    public void setArrAptCodeIata(String arrAptCodeIata) {
        this.arrAptCodeIata = arrAptCodeIata;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public Aenaflight baggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
        return this;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public String getCarrierAirlineNameEn() {
        return carrierAirlineNameEn;
    }

    public Aenaflight carrierAirlineNameEn(String carrierAirlineNameEn) {
        this.carrierAirlineNameEn = carrierAirlineNameEn;
        return this;
    }

    public void setCarrierAirlineNameEn(String carrierAirlineNameEn) {
        this.carrierAirlineNameEn = carrierAirlineNameEn;
    }

    public String getCarrierIcaoCode() {
        return carrierIcaoCode;
    }

    public Aenaflight carrierIcaoCode(String carrierIcaoCode) {
        this.carrierIcaoCode = carrierIcaoCode;
        return this;
    }

    public void setCarrierIcaoCode(String carrierIcaoCode) {
        this.carrierIcaoCode = carrierIcaoCode;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public Aenaflight carrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
        return this;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    public String getCounter() {
        return counter;
    }

    public Aenaflight counter(String counter) {
        this.counter = counter;
        return this;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getDepAptNameEs() {
        return depAptNameEs;
    }

    public Aenaflight depAptNameEs(String depAptNameEs) {
        this.depAptNameEs = depAptNameEs;
        return this;
    }

    public void setDepAptNameEs(String depAptNameEs) {
        this.depAptNameEs = depAptNameEs;
    }

    public String getDepAptCodeIata() {
        return depAptCodeIata;
    }

    public Aenaflight depAptCodeIata(String depAptCodeIata) {
        this.depAptCodeIata = depAptCodeIata;
        return this;
    }

    public void setDepAptCodeIata(String depAptCodeIata) {
        this.depAptCodeIata = depAptCodeIata;
    }

    public String getEstArrDateTimeLt() {
        return estArrDateTimeLt;
    }

    public Aenaflight estArrDateTimeLt(String estArrDateTimeLt) {
        this.estArrDateTimeLt = estArrDateTimeLt;
        return this;
    }

    public void setEstArrDateTimeLt(String estArrDateTimeLt) {
        this.estArrDateTimeLt = estArrDateTimeLt;
    }

    public String getEstDepDateTimeLt() {
        return estDepDateTimeLt;
    }

    public Aenaflight estDepDateTimeLt(String estDepDateTimeLt) {
        this.estDepDateTimeLt = estDepDateTimeLt;
        return this;
    }

    public void setEstDepDateTimeLt(String estDepDateTimeLt) {
        this.estDepDateTimeLt = estDepDateTimeLt;
    }

    public String getFlightAirlineNameEn() {
        return flightAirlineNameEn;
    }

    public Aenaflight flightAirlineNameEn(String flightAirlineNameEn) {
        this.flightAirlineNameEn = flightAirlineNameEn;
        return this;
    }

    public void setFlightAirlineNameEn(String flightAirlineNameEn) {
        this.flightAirlineNameEn = flightAirlineNameEn;
    }

    public String getFlightAirlineName() {
        return flightAirlineName;
    }

    public Aenaflight flightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
        return this;
    }

    public void setFlightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
    }

    public String getFlightIcaoCode() {
        return flightIcaoCode;
    }

    public Aenaflight flightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
        return this;
    }

    public void setFlightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public Aenaflight flightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public Aenaflight fltLegSeqNo(String fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
        return this;
    }

    public void setFltLegSeqNo(String fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public Aenaflight gateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
        return this;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public Aenaflight loungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
        return this;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    public String getSchdArrOnlyDateLt() {
        return schdArrOnlyDateLt;
    }

    public Aenaflight schdArrOnlyDateLt(String schdArrOnlyDateLt) {
        this.schdArrOnlyDateLt = schdArrOnlyDateLt;
        return this;
    }

    public void setSchdArrOnlyDateLt(String schdArrOnlyDateLt) {
        this.schdArrOnlyDateLt = schdArrOnlyDateLt;
    }

    public String getSchdArrOnlyTimeLt() {
        return schdArrOnlyTimeLt;
    }

    public Aenaflight schdArrOnlyTimeLt(String schdArrOnlyTimeLt) {
        this.schdArrOnlyTimeLt = schdArrOnlyTimeLt;
        return this;
    }

    public void setSchdArrOnlyTimeLt(String schdArrOnlyTimeLt) {
        this.schdArrOnlyTimeLt = schdArrOnlyTimeLt;
    }

    public String getSourceData() {
        return sourceData;
    }

    public Aenaflight sourceData(String sourceData) {
        this.sourceData = sourceData;
        return this;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public Aenaflight statusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
        return this;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public Aenaflight terminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
        return this;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public Aenaflight arrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
        return this;
    }

    public void setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Aenaflight createdAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getActDepDateTimeLt() {
        return actDepDateTimeLt;
    }

    public Aenaflight actDepDateTimeLt(String actDepDateTimeLt) {
        this.actDepDateTimeLt = actDepDateTimeLt;
        return this;
    }

    public void setActDepDateTimeLt(String actDepDateTimeLt) {
        this.actDepDateTimeLt = actDepDateTimeLt;
    }

    public String getSchdDepOnlyDateLt() {
        return schdDepOnlyDateLt;
    }

    public Aenaflight schdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
        return this;
    }

    public void setSchdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
    }

    public String getSchdDepOnlyTimeLt() {
        return schdDepOnlyTimeLt;
    }

    public Aenaflight schdDepOnlyTimeLt(String schdDepOnlyTimeLt) {
        this.schdDepOnlyTimeLt = schdDepOnlyTimeLt;
        return this;
    }

    public void setSchdDepOnlyTimeLt(String schdDepOnlyTimeLt) {
        this.schdDepOnlyTimeLt = schdDepOnlyTimeLt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Aenaflight aenaflight = (Aenaflight) o;
        if (aenaflight.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aenaflight.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Aenaflight{" +
            "id=" + getId() +
            ", actArrDateTimeLt='" + getActArrDateTimeLt() + "'" +
            ", aircraftNameScheduled='" + getAircraftNameScheduled() + "'" +
            ", arrAptNameEs='" + getArrAptNameEs() + "'" +
            ", arrAptCodeIata='" + getArrAptCodeIata() + "'" +
            ", baggageInfo='" + getBaggageInfo() + "'" +
            ", carrierAirlineNameEn='" + getCarrierAirlineNameEn() + "'" +
            ", carrierIcaoCode='" + getCarrierIcaoCode() + "'" +
            ", carrierNumber='" + getCarrierNumber() + "'" +
            ", counter='" + getCounter() + "'" +
            ", depAptNameEs='" + getDepAptNameEs() + "'" +
            ", depAptCodeIata='" + getDepAptCodeIata() + "'" +
            ", estArrDateTimeLt='" + getEstArrDateTimeLt() + "'" +
            ", estDepDateTimeLt='" + getEstDepDateTimeLt() + "'" +
            ", flightAirlineNameEn='" + getFlightAirlineNameEn() + "'" +
            ", flightAirlineName='" + getFlightAirlineName() + "'" +
            ", flightIcaoCode='" + getFlightIcaoCode() + "'" +
            ", flightNumber='" + getFlightNumber() + "'" +
            ", fltLegSeqNo='" + getFltLegSeqNo() + "'" +
            ", gateInfo='" + getGateInfo() + "'" +
            ", loungeInfo='" + getLoungeInfo() + "'" +
            ", schdArrOnlyDateLt='" + getSchdArrOnlyDateLt() + "'" +
            ", schdArrOnlyTimeLt='" + getSchdArrOnlyTimeLt() + "'" +
            ", sourceData='" + getSourceData() + "'" +
            ", statusInfo='" + getStatusInfo() + "'" +
            ", terminalInfo='" + getTerminalInfo() + "'" +
            ", arrTerminalInfo='" + getArrTerminalInfo() + "'" +
            ", createdAt=" + getCreatedAt() +
            ", actDepDateTimeLt='" + getActDepDateTimeLt() + "'" +
            ", schdDepOnlyDateLt='" + getSchdDepOnlyDateLt() + "'" +
            ", schdDepOnlyTimeLt='" + getSchdDepOnlyTimeLt() + "'" +
            "}";
    }
}
