package com.test.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.test.service.util.ProcessorUtil;
import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;

@Entity
@Table(name = "aenaflight_temp")
public class AenaflightTemp implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private Key key;

    @Size(max = 64)
    @Column(name = "act_arr_date_time_lt", length = 64)
    private String actArrDateTimeLt;

    @Column(columnDefinition="TEXT", name = "aircraft_name_scheduled")
    private String aircraftNameScheduled;

    @Size(max = 128)
    @Column(name = "arr_apt_name_es", length = 128)
    private String arrAptNameEs;

    @Size(max = 8)
    @Column(name = "arr_apt_code_iata", length = 8)
    private String arrAptCodeIata;

    @Column(columnDefinition="TEXT", name = "baggage_info")
    private String baggageInfo;

    @Size(max = 128)
    @Column(name = "carrier_airline_name_en", length = 128)
    private String carrierAirlineNameEn;

    @Size(max = 8)
    @Column(name = "carrier_icao_code", length = 8)
    private String carrierIcaoCode;

    @Size(max = 8)
    @Column(name = "carrier_number", length = 8)
    private String carrierNumber;

    @Column(columnDefinition="TEXT", name = "counter")
    private String counter;

    @Size(max = 128)
    @Column(name = "dep_apt_name_es", length = 128)
    private String depAptNameEs;

    @Size(max = 8)
    @Column(name = "dep_apt_code_iata", length = 8)
    private String depAptCodeIata;

    @Size(max = 64)
    @Column(name = "est_arr_date_time_lt", length = 64)
    private String estArrDateTimeLt;

    @Size(max = 64)
    @Column(name = "est_dep_date_time_lt", length = 64)
    private String estDepDateTimeLt;

    @Size(max = 128)
    @Column(name = "flight_airline_name_en", length = 128)
    private String flightAirlineNameEn;

    @Size(max = 128)
    @Column(name = "flight_airline_name", length = 128)
    private String flightAirlineName;

    @Size(max = 8)
    @Column(name = "flt_leg_seq_no", length = 8)
    private String fltLegSeqNo;

    @Column(columnDefinition="TEXT", name = "gate_info")
    private String gateInfo;

    @Column(columnDefinition="TEXT", name = "lounge_info")
    private String loungeInfo;

    @Size(max = 32)
    @Column(name = "schd_arr_only_date_lt", length = 32)
    private String schdArrOnlyDateLt;

    @Size(max = 32)
    @Column(name = "schd_arr_only_time_lt", length = 32)
    private String schdArrOnlyTimeLt;

    @Column(columnDefinition="TEXT", name = "source_data")
    private String sourceData;

    @Size(max = 128)
    @Column(name = "status_info", length = 128)
    private String statusInfo;

    @Column(columnDefinition="TEXT", name = "terminal_info")
    private String terminalInfo;

    @Column(columnDefinition="TEXT", name = "arr_terminal_info")
    private String arrTerminalInfo;

    @Column(name = "created_at")
    private Long createdAt;

    @Size(max = 64)
    @Column(name = "act_dep_date_time_lt", length = 64)
    private String actDepDateTimeLt;

    @Size(max = 32)
    @Column(name = "schd_dep_only_time_lt", length = 32)
    private String schdDepOnlyTimeLt;

    public static AenaflightTemp toTempValue(Aenaflight entity) {
        AenaflightTemp temp = new AenaflightTemp();
        temp.setActArrDateTimeLt(entity.getActArrDateTimeLt());
        temp.setArrAptCodeIata(entity.getArrAptCodeIata());
        temp.setAircraftNameScheduled(entity.getAircraftNameScheduled());
        temp.setArrAptNameEs(entity.getArrAptNameEs());
        temp.setBaggageInfo(ProcessorUtil.notEmptyAndNotNull(entity.getBaggageInfo()) ? entity.getBaggageInfo() : "");
        temp.setCarrierNumber(entity.getCarrierNumber());
        temp.setCarrierIcaoCode(entity.getCarrierIcaoCode());
        temp.setCarrierAirlineNameEn(entity.getCarrierAirlineNameEn());
        temp.setCounter(ProcessorUtil.notEmptyAndNotNull(entity.getCounter()) ? entity.getCounter() : "");
        temp.setDepAptCodeIata(entity.getDepAptCodeIata());
        temp.setDepAptNameEs(entity.getDepAptNameEs());
        temp.setEstArrDateTimeLt(entity.getEstArrDateTimeLt());
        temp.setEstDepDateTimeLt(entity.getEstDepDateTimeLt());
        temp.setFlightAirlineName(entity.getFlightAirlineName());
        temp.setFlightAirlineNameEn(entity.getFlightAirlineNameEn());
        temp.setFltLegSeqNo(entity.getFltLegSeqNo());
        temp.setGateInfo(ProcessorUtil.notEmptyAndNotNull(entity.getGateInfo()) ? entity.getGateInfo() : "");
        temp.setLoungeInfo(ProcessorUtil.notEmptyAndNotNull(entity.getLoungeInfo()) ? entity.getLoungeInfo() : "");
        temp.setSchdArrOnlyDateLt(entity.getSchdArrOnlyDateLt());
        temp.setSchdArrOnlyTimeLt(entity.getSchdArrOnlyTimeLt());
        temp.setSchdDepOnlyTimeLt(entity.getSchdDepOnlyTimeLt());
        temp.setSourceData(entity.getSourceData());
        temp.setStatusInfo(entity.getStatusInfo());
        temp.setTerminalInfo(ProcessorUtil.notEmptyAndNotNull(entity.getTerminalInfo()) ? entity.getTerminalInfo() : "");
        temp.setArrTerminalInfo(ProcessorUtil.notEmptyAndNotNull(entity.getArrTerminalInfo()) ? entity.getArrTerminalInfo() : "");
        temp.setActDepDateTimeLt(entity.getActDepDateTimeLt());
        Key key = new Key();
        key.setFlightNumber(entity.getFlightNumber());
        key.setFlightIcaoCode(entity.getFlightIcaoCode());
        key.setSchdDepOnlyDateLt(entity.getSchdDepOnlyDateLt());
        temp.setKey(key);
        return temp;
    }

    public String getActArrDateTimeLt() {
        return actArrDateTimeLt;
    }

    public void setActArrDateTimeLt(String actArrDateTimeLt) {
        this.actArrDateTimeLt = actArrDateTimeLt;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    public String getArrAptNameEs() {
        return arrAptNameEs;
    }

    public void setArrAptNameEs(String arrAptNameEs) {
        this.arrAptNameEs = arrAptNameEs;
    }

    public String getArrAptCodeIata() {
        return arrAptCodeIata;
    }

    public void setArrAptCodeIata(String arrAptCodeIata) {
        this.arrAptCodeIata = arrAptCodeIata;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public String getCarrierAirlineNameEn() {
        return carrierAirlineNameEn;
    }

    public void setCarrierAirlineNameEn(String carrierAirlineNameEn) {
        this.carrierAirlineNameEn = carrierAirlineNameEn;
    }

    public String getCarrierIcaoCode() {
        return carrierIcaoCode;
    }

    public void setCarrierIcaoCode(String carrierIcaoCode) {
        this.carrierIcaoCode = carrierIcaoCode;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getDepAptNameEs() {
        return depAptNameEs;
    }

    public void setDepAptNameEs(String depAptNameEs) {
        this.depAptNameEs = depAptNameEs;
    }

    public String getDepAptCodeIata() {
        return depAptCodeIata;
    }

    public void setDepAptCodeIata(String depAptCodeIata) {
        this.depAptCodeIata = depAptCodeIata;
    }

    public String getEstArrDateTimeLt() {
        return estArrDateTimeLt;
    }

    public void setEstArrDateTimeLt(String estArrDateTimeLt) {
        this.estArrDateTimeLt = estArrDateTimeLt;
    }

    public String getEstDepDateTimeLt() {
        return estDepDateTimeLt;
    }

    public void setEstDepDateTimeLt(String estDepDateTimeLt) {
        this.estDepDateTimeLt = estDepDateTimeLt;
    }

    public String getFlightAirlineNameEn() {
        return flightAirlineNameEn;
    }

    public void setFlightAirlineNameEn(String flightAirlineNameEn) {
        this.flightAirlineNameEn = flightAirlineNameEn;
    }

    public String getFlightAirlineName() {
        return flightAirlineName;
    }

    public void setFlightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
    }

    public String getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public void setFltLegSeqNo(String fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    public String getSchdArrOnlyDateLt() {
        return schdArrOnlyDateLt;
    }

    public void setSchdArrOnlyDateLt(String schdArrOnlyDateLt) {
        this.schdArrOnlyDateLt = schdArrOnlyDateLt;
    }

    public String getSchdArrOnlyTimeLt() {
        return schdArrOnlyTimeLt;
    }

    public void setSchdArrOnlyTimeLt(String schdArrOnlyTimeLt) {
        this.schdArrOnlyTimeLt = schdArrOnlyTimeLt;
    }

    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public void setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getActDepDateTimeLt() {
        return actDepDateTimeLt;
    }

    public void setActDepDateTimeLt(String actDepDateTimeLt) {
        this.actDepDateTimeLt = actDepDateTimeLt;
    }

    public String getSchdDepOnlyTimeLt() {
        return schdDepOnlyTimeLt;
    }

    public void setSchdDepOnlyTimeLt(String schdDepOnlyTimeLt) {
        this.schdDepOnlyTimeLt = schdDepOnlyTimeLt;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }
}
