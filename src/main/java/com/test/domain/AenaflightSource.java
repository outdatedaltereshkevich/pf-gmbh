package com.test.domain;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "aenaflight_source")
public class AenaflightSource implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="aenaflight_source_id_seq")
    @SequenceGenerator(name="aenaflight_source_id_seq", sequenceName="aenaflight_source_id_seq", allocationSize=1)
    private Long id;

    @Size(max = 8)
    @Column(name = "adep", length = 8)
    private String adep;

    @Size(max = 8)
    @Column(name = "ades", length = 8)
    private String ades;

    @Size(max = 8)
    @Column(name = "flight_code", length = 8)
    private String flightCode;

    @Size(max = 8)
    @Column(name = "flight_number", length = 8)
    private String flightNumber;

    @Size(max = 8)
    @Column(name = "carrier_code", length = 8)
    private String carrierCode;

    @Size(max = 8)
    @Column(name = "carrier_number", length = 8)
    private String carrierNumber;

    @Size(max = 256)
    @Column(name = "status_info", length = 256)
    private String statusInfo;

    @Column(name = "schd_dep_lt")
    private LocalDateTime schdDepLt;

    @Column(name = "schd_arr_lt")
    private LocalDateTime schdArrLt;

    @Column(name = "est_dep_lt")
    private LocalDateTime estDepLt;

    @Column(name = "est_arr_lt")
    private LocalDateTime estArrLt;

    @Column(name = "act_dep_lt")
    private LocalDateTime actDepLt;

    @Column(name = "act_arr_lt")
    private LocalDateTime actArrLt;

    @Column(name="flt_leg_seq_no")
    private Integer fltLegSeqNo;

    @Column(columnDefinition="TEXT", name = "aircraft_name_scheduled")
    private String aircraftNameScheduled;

    @Size(max = 2000)
    @Column(name = "baggage_info", length = 2000)
    private String baggageInfo;

    @Size(max = 2000)
    @Column(name = "counter", length = 2000)
    private String counter;

    @Size(max = 2000)
    @Column(name = "gate_info", length = 2000)
    private String gateInfo;

    @Size(max = 2000)
    @Column(name = "lounge_info", length = 2000)
    private String loungeInfo;

    @Size(max = 2000)
    @Column(name = "terminal_info", length = 2000)
    private String terminalInfo;

    @Size(max = 2000)
    @Column(name = "arr_terminal_info", length = 2000)
    private String arrTerminalInfo;

    @Column(columnDefinition="TEXT", name = "source_data")
    private String sourceData;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdep() {
        return adep;
    }

    public void setAdep(String adep) {
        this.adep = adep;
    }

    public String getAdes() {
        return ades;
    }

    public void setAdes(String ades) {
        this.ades = ades;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public LocalDateTime getSchdDepLt() {
        return schdDepLt;
    }

    public void setSchdDepLt(LocalDateTime schdDepLt) {
        this.schdDepLt = schdDepLt;
    }

    public LocalDateTime getSchdArrLt() {
        return schdArrLt;
    }

    public void setSchdArrLt(LocalDateTime schdArrLt) {
        this.schdArrLt = schdArrLt;
    }

    public LocalDateTime getEstDepLt() {
        return estDepLt;
    }

    public void setEstDepLt(LocalDateTime estDepLt) {
        this.estDepLt = estDepLt;
    }

    public LocalDateTime getEstArrLt() {
        return estArrLt;
    }

    public void setEstArrLt(LocalDateTime estArrLt) {
        this.estArrLt = estArrLt;
    }

    public LocalDateTime getActDepLt() {
        return actDepLt;
    }

    public void setActDepLt(LocalDateTime actDepLt) {
        this.actDepLt = actDepLt;
    }

    public LocalDateTime getActArrLt() {
        return actArrLt;
    }

    public void setActArrLt(LocalDateTime actArrLt) {
        this.actArrLt = actArrLt;
    }

    public Integer getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public void setFltLegSeqNo(Integer fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public void setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
    }

    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
