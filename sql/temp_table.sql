CREATE TABLE public.aenaflight_temp
(
  act_arr_date_time_lt character varying(64),
  aircraft_name_scheduled text,
  arr_apt_name_es character varying(128),
  arr_apt_code_iata character varying(8),
  baggage_info text,
  carrier_airline_name_en character varying(128),
  carrier_icao_code character varying(8),
  carrier_number character varying(8),
  counter text,
  dep_apt_name_es character varying(128),
  dep_apt_code_iata character varying(8),
  est_arr_date_time_lt character varying(64),
  est_dep_date_time_lt character varying(64),
  flight_airline_name_en character varying(128),
  flight_airline_name character varying(128),
  flight_icao_code character varying(8) NOT NULL,
  flight_number character varying(8) NOT NULL,
  flt_leg_seq_no character varying(8),
  gate_info text,
  lounge_info text,
  schd_arr_only_date_lt character varying(32),
  schd_arr_only_time_lt character varying(32),
  source_data text,
  status_info character varying(128),
  terminal_info text,
  arr_terminal_info text,
  created_at bigint,
  act_dep_date_time_lt character varying(64),
  schd_dep_only_date_lt character varying(32),
  schd_dep_only_time_lt character varying(32),
  CONSTRAINT aenaflight_temp_pkey PRIMARY KEY (flight_icao_code, flight_number)
);

CREATE TABLE public.aenaflight_source
(
  id bigserial NOT NULL PRIMARY KEY,
  adep character varying(8) NOT NULL,
  ades character varying(8) NOT NULL,
  flight_code character varying(8) NOT NULL,
  flight_number character varying(8) NOT NULL,
  carrier_code character varying(8),
  carrier_number character varying(8),
  status_info character varying(256) NOT NULL,
  schd_dep_lt timestamp without time zone NOT NULL,
  schd_arr_lt timestamp without time zone NOT NULL,
  est_dep_lt timestamp without time zone,
  est_arr_lt timestamp without time zone,
  act_dep_lt timestamp without time zone,
  act_arr_lt timestamp without time zone,
  flt_leg_seq_no integer NOT NULL,
  aircraft_name_scheduled text,
  baggage_info character varying(128),
  counter character varying(128),
  gate_info character varying(128),
  lounge_info character varying(128),
  terminal_info character varying(128),
  arr_terminal_info character varying(128),
  source_data text,
  created_at timestamp without time zone NOT NULL,
  CONSTRAINT aenaflight_source_pkey PRIMARY KEY (id)
);

CREATE TABLE public.metadata
(
  id bigserial NOT NULL PRIMARY KEY,
  value bigint,
  key character varying,
  CONSTRAINT metadata_pkey PRIMARY KEY (id)
);